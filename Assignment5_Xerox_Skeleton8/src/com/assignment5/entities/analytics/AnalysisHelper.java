/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.assignment5.entities.analytics;

import com.assignment5.entities.Customer;
import com.assignment5.entities.Order;
import com.assignment5.entities.Product;
import com.assignment5.entities.SalesPerson;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

/**
 *
 * @author Team Void
 */
public class AnalysisHelper {
   
    /*public void checkOrder()
    {
     Map< Integer, Order> orderMap = DataStore.getInstance().getOrder();
    for (Order order : orderMap.values()) {

            System.out.println("Order ID "+order.getOrderId() + "Product ID "+ order.getItem().getProductId()+ " Quantity " + order.getItem().getQuantity()+ " Custimer id "+ order.getCustomerId()+" Sales Price "+ order.getItem().getSalesPrice());
    }
    }*/

    public void threeBestNegotiatedProducts() {

        Map< Integer, Product> productsList = DataStore.getInstance().getProduct();
        Map< Integer, Order> orderMap = DataStore.getInstance().getOrder();
        Map< Integer, Integer> result = new HashMap<>();

        for (Order order : orderMap.values()) {

            int count = 0;
            int productID = order.getItem().getProductId();
            int target = 0;
            int quant = order.getItem().getQuantity();

            for (Product p : productsList.values()) {
                if (p.getProductId() == productID) {
                    target = p.getTargetPrice();
                }
            }

            if (order.getItem().getSalesPrice() > target) {

                if (result.containsKey(productID)) {

                    count = result.get(productID); //returns the value for given key
                    quant += count;

                }

                result.put(productID, quant);
            }
        }

        Set entrySet = result.entrySet();
        List< Entry< Integer, Integer>> list = new ArrayList<>(entrySet);
        Collections.sort(list, new Comparator< Entry< Integer, Integer>>() {

            public int compare(Entry< Integer, Integer> a, Entry< Integer, Integer> b) {

                return b.getValue() - a.getValue();
            }
        });
        System.out.println("*********************************************************************");
        System.out.println("(1) Top three negotiated products:");

        for (int i = 0; i < list.size() && i < 3; i++) {
            int productId = list.get(i).getKey();
            int quant = list.get(i).getValue();
            System.out.println("ProductID: " + productId + " Quantity: " + quant);
        }

        System.out.println("*********************************************************************");

    }
    
    
    
    public void topThreeCustomers() {

        Map< Integer, Product> productsList = DataStore.getInstance().getProduct();
        Map< Integer, Customer> customerMap = DataStore.getInstance().getCustomer();
        Map< Integer, Integer> result = new HashMap<>();
        Map< Integer, Order> orderMap = DataStore.getInstance().getOrder();
       
     //   for(Customer c: customerMap.values()){
        
        for (Order order : orderMap.values()) {

            int count = 0;
            int productID = order.getItem().getProductId();
            int customerID = order.getCustomerId();
            int sales = order.getItem().getSalesPrice();
            int target = 0;

            for (Product p : productsList.values()) {
                if (p.getProductId() == productID) {
                    target = p.getTargetPrice();
                }
            }
            int diff = Math.abs(sales - target);
            
            if (result.containsKey(customerID)) {

                count = result.get(customerID); //returns the value for given key
                diff += count;

            }
                result.put(customerID, diff);
            }
        
        Set entrySet = result.entrySet();
        List< Entry< Integer, Integer>> list = new ArrayList<>(entrySet);
        Collections.sort(list, new Comparator< Entry< Integer, Integer>>() {
            public int compare(Entry< Integer, Integer> a, Entry< Integer, Integer> b) {

                return a.getValue() - b.getValue();
            }
        });
        System.out.println("(2) Top three customers:");

        for (int i = 0; i < list.size() && i < 3; i++) {
            int custID = list.get(i).getKey();
            int quant = list.get(i).getValue();
            System.out.println("CustomerID: " + custID + " Profit: " + quant);
        }

        System.out.println("*********************************************************************");
    }
    
    public void topThreeSales() {

        Map< Integer, Product> productsList = DataStore.getInstance().getProduct();
        Map< Integer, Order> orderMap = DataStore.getInstance().getOrder();
        Map< Integer, Integer> result = new HashMap<>();

        for (Order order : orderMap.values()) {

            int count = 0;
            int productID = order.getItem().getProductId();
            int salesID = order.getSupplierId();
            int sales = order.getItem().getSalesPrice();
            int target = 0;

            for (Product p : productsList.values()) {
                if (p.getProductId() == productID) {
                    target = p.getTargetPrice();
                }
            }
            int diff = sales - target;
            int quantsales=0;
            if(diff>0)
            {
            quantsales = diff * order.getItem().getQuantity();
            if (result.containsKey(salesID)) {

                count = result.get(salesID); //returns the value for given key
                quantsales +=count;
            }

            result.put(salesID, quantsales);
            }
        }
        

        Set entrySet = result.entrySet();
        List< Entry< Integer, Integer>> list = new ArrayList<>(entrySet);
        Collections.sort(list, new Comparator< Entry< Integer, Integer>>() {
            @Override
            public int compare(Entry< Integer, Integer> a, Entry< Integer, Integer> b) {

                return b.getValue() - a.getValue();
            }
        });
        System.out.println("(3) Top three salesperson:");

        for (int i = 0; i < list.size() && i < 3; i++) {
            int salesID = list.get(i).getKey();
            int quantsales = list.get(i).getValue();
            System.out.println("Sales ID: " + salesID + " Profit: " + quantsales);
        }

        System.out.println("*********************************************************************");
    }
     
    
    
    public void totalrevenue() {

        Map< Integer, Product> productsList = DataStore.getInstance().getProduct();
        Map< Integer, Order> orderMap = DataStore.getInstance().getOrder();
        Map< Integer, Integer> targetPrice = new HashMap<>();
        Map< Integer, Integer> result = new HashMap<>();
        int total=0;
        
        for (Order order : orderMap.values()) {

            //int count = 0;
            int productID = order.getItem().getProductId();
            //int salesID = order.getSupplierId();
            int sales = order.getItem().getSalesPrice();
            int target = 0;

            for (Product p : productsList.values()) {
                if (p.getProductId() == productID) {
                    target = p.getTargetPrice();
                }
            }
            int diff = sales - target;
            
            if(diff>0)
            {
            int quantsales = diff * order.getItem().getQuantity();
            total+=quantsales;
            }
        }
        System.out.println("(4) Total revenue:"+ total);
        System.out.println("*********************************************************************");
    }

    

/*
    public void topThreeCustomers() {

        Map< Integer, Product> productsList = DataStore.getInstance().getProduct();
        Map< Integer, Integer> result = new HashMap<>();
        Map< Integer, Order> orderMap = DataStore.getInstance().getOrder();
       
       
        
        for (Order order : orderMap.values()) {

            int count = 0;
            int productID = order.getItem().getProductId();
            int customerID = order.getCustomerId();
            int sales = order.getItem().getSalesPrice();
            int target = 0;

            for (Product p : productsList.values()) {
                if (p.getProductId() == productID) {
                    target = p.getTargetPrice();
                }
            }
            int diff = Math.abs(sales - target);
            
            if (result.containsKey(customerID)) {

                count = result.get(customerID); //returns the value for given key
                diff += count;

            }
                result.put(customerID, diff);
               // System.out.println("Display"+result);
            
        }

        Set entrySet = result.entrySet();
        List< Entry< Integer, Integer>> list = new ArrayList<>(entrySet);
        Collections.sort(list, new Comparator< Entry< Integer, Integer>>() {
            public int compare(Entry< Integer, Integer> a, Entry< Integer, Integer> b) {

                return a.getValue() - b.getValue();
            }
        });
        System.out.println("Top three customers:");

        for (int i = 0; i < list.size() && i < 3; i++) {
            int custID = list.get(i).getKey();
            int quant = list.get(i).getValue();
            System.out.println("CustomerID: " + custID + " Profit: " + quant);
        }

        System.out.println("*********************************************************************");
    }
    
    public void topThreeSales() {

        Map< Integer, Product> productsList = DataStore.getInstance().getProduct();
        Map< Integer, Order> orderMap = DataStore.getInstance().getOrder();
       
        Map< Integer, Integer> result = new HashMap<>();

        
        
        for (Order order : orderMap.values()) {

            int count = 0;
            int productID = order.getItem().getProductId();
            int salesID = order.getSupplierId();
            int sales = order.getItem().getSalesPrice();
            int target = 0;

            for (Product p : productsList.values()) {
                if (p.getProductId() == productID) {
                    target = p.getTargetPrice();
                }
            }
            int diff = sales - target;
            int quantsales=0;
            quantsales = diff * order.getItem().getQuantity();
            
            
            if(diff>0)
            {
            if (result.containsKey(salesID)) {

                count = result.get(salesID); //returns the value for given key
                quantsales +=count;
            }

            result.put(salesID, quantsales);
            }
        }
        

        Set entrySet = result.entrySet();
        List< Entry< Integer, Integer>> list = new ArrayList<>(entrySet);
        Collections.sort(list, new Comparator< Entry< Integer, Integer>>() {
            @Override
            public int compare(Entry< Integer, Integer> a, Entry< Integer, Integer> b) {

                return b.getValue() - a.getValue();
            }
        });
        System.out.println("Top three salesperson:");

        for (int i = 0; i < list.size() && i < 3; i++) {
            int salesID = list.get(i).getKey();
            int quantsales = list.get(i).getValue();
            System.out.println("Sales ID: " + salesID + " Profit: " + quantsales);
        }

        System.out.println("*********************************************************************");
    }
     
    
    
    public void totalrevenue() {

        Map< Integer, Product> productsList = DataStore.getInstance().getProduct();
        Map< Integer, Order> orderMap = DataStore.getInstance().getOrder();
       // Map< Integer, Integer> targetPrice = new HashMap<>();
        //Map< Integer, SalesPerson> salesMap = DataStore.getInstance().getSalesPerson();
        Map< Integer, Integer> result = new HashMap<>();
         
         
         
         for (Order order : orderMap.values()) {

            int count = 0;
            int productID = order.getItem().getProductId();
            int salesID = order.getSupplierId();
            int sales = order.getItem().getSalesPrice();
            int target = 0;

            for (Product p : productsList.values()) {
                if (p.getProductId() == productID) {
                    target = p.getTargetPrice();
                }
            }
            int diff = sales - target;
            int quantsales = diff * order.getItem().getQuantity();
            
            if(diff>0)
            {
            if (result.containsKey(salesID)) {

                count = result.get(salesID); //returns the value for given key
                quantsales +=count;
            }

            result.put(salesID, quantsales);
            }
         }
        

        Set entrySet = result.entrySet();
        List< Entry< Integer, Integer>> list = new ArrayList<>(entrySet);
        Collections.sort(list, new Comparator< Entry< Integer, Integer>>() {
            @Override
            public int compare(Entry< Integer, Integer> a, Entry< Integer, Integer> b) {

                return b.getValue() - a.getValue();
            }
        });
        System.out.println("Total revenue:");
        int quantsalesfinal=0;

        for (int i = 0; i < list.size(); i++) {
          //  int salesID = list.get(i).getKey();
            int quantsales = list.get(i).getValue();
            quantsalesfinal= quantsalesfinal + quantsales;   
        }
         System.out.println("Total revenue is " + quantsalesfinal);
        System.out.println("*********************************************************************");
    }*/

     public void finalMethod(){
         
         Map< Integer, Product> productsMap = DataStore.getInstance().getProduct();
         Map< Integer, Order> orderMap = DataStore.getInstance().getOrder();
         Map< Integer, Double> avgMap = new HashMap<>();
         Map< Integer, Integer> targetMap = new HashMap<>();
         Map< Integer, Double> differenceMap = new HashMap<>();
         Map< Integer, Double> adjustedTargetMap = new HashMap<>();
         Map< Integer, Double> adjustedDifferenceMap = new HashMap<>();
         Map< Integer, Double> errorMap = new HashMap<>();
         
         //****************** Sorting Final **************************
         Map< Integer, Double> originalLower = new HashMap<>();
         Map< Integer, Double> originalHigher = new HashMap<>();
         Map< Integer, Double> modLower = new HashMap<>();
         Map< Integer, Double> modHigher = new HashMap<>();

             for (Product p : productsMap.values()) {
                 
             double sumSales = 0;
             double sumQuantity = 0;
             
             for(Order o: orderMap.values()){
                   
                if (p.getProductId() == o.getItem().getProductId()) {
                
                    sumSales += o.getItem().getSalesPrice() * o.getItem().getQuantity();
                    sumQuantity += o.getItem().getQuantity();
                    
                }   
               }
             double difference = 0;
             double averagePrice = 0;
             averagePrice = sumSales/sumQuantity;
             difference = averagePrice - p.getTargetPrice();
             avgMap.put(p.getProductId(), averagePrice);
             targetMap.put(p.getProductId(), p.getTargetPrice()); 
             differenceMap.put(p.getProductId(), difference);
             
             //*********************** Modified Data ***********************
        
            double adjustedTarget = 0;
            
            if(p.getTargetPrice() < (averagePrice - (averagePrice * 0.05))){
                
                adjustedTarget = averagePrice - (averagePrice * 0.05);
                adjustedTargetMap.put(p.getProductId(), adjustedTarget);
            }
            
            else{
                
                adjustedTarget = averagePrice + (averagePrice * 0.05);
                adjustedTargetMap.put(p.getProductId(), adjustedTarget);
            }
            
            double adjustedDiff =0;
            adjustedDiff = averagePrice - adjustedTarget;
            double errorValue = 0;
            
            errorValue = (adjustedTarget - averagePrice)/averagePrice;
            adjustedDifferenceMap.put(p.getProductId(), adjustedDiff);
            errorMap.put(p.getProductId(), errorValue);
            
            //************ Separating Table Logic **********************
            
            if(averagePrice < p.getTargetPrice()){  
               originalLower.put(p.getProductId(), averagePrice);
            }
            else{
                originalHigher.put(p.getProductId(), averagePrice);
            }
               
            if(averagePrice < adjustedTarget){
                modLower.put(p.getProductId(), averagePrice);
            }
            else{   
                modHigher.put(p.getProductId(), averagePrice);
            }
        }  
             
        //************************Display Table Logic**********************
        
        System.out.println("ORIGINAL DATA:\n");
        System.out.println("-------------------------------------------------------------------------------");
        System.out.println("ProductId"+"\t"+"Average Sales Price"+"\t"+"Target Price"+"\t"+"Difference Price");
        System.out.println("-------------------------------------------------------------------------------");
        
        Set entrySet1 = avgMap.entrySet();
        Set entrySet2 = targetMap.entrySet();
        Set entrySet3 = differenceMap.entrySet();
        
        //*************** Modified Table ****************
        
        Set entrySet4 = adjustedTargetMap.entrySet();
        Set entrySet5 = adjustedDifferenceMap.entrySet();
        Set entrySet6 = errorMap.entrySet();
        
        //*************** Separated Values *************
        
        Set entrySet7 = originalLower.entrySet();
        Set entrySet8 = originalHigher.entrySet();
        Set entrySet9 = modLower.entrySet();
        Set entrySet10 = modHigher.entrySet();
        
        List< Entry< Integer, Double>> avglist = new ArrayList<>(entrySet1);
        List< Entry< Integer, Integer>> targetlist = new ArrayList<>(entrySet2);
        List< Entry< Integer, Double>> differentList = new ArrayList<>(entrySet3);
        
        //Object arr[] = differenceMap.entrySet().toArray();
        
      /*  Collections.sort(differentList, new Comparator< Entry< Integer, Double>>() {
            @Override
            public int compare(Entry< Integer, Double> a, Entry< Integer, Double> b) {

                return (int)(b.getValue() - a.getValue());
            }
        });
        
        int prodId = 0;
        double agprice = 0;
        int tarprice = 0;
        double diferen = 0;
            
      //System.out.println("list not"+differenceMap);  
       // System.out.println("list sorted"+differentList);
        for (int i = 0; i < differentList.size(); i++) {

            for(int j = 0; j < targetlist.size(); j++){
                
                for(int k = 0; k < avglist.size(); k++){
                    
                    if(differentList.get(i).getKey() == targetlist.get(j).getKey() && targetlist.get(j).getKey() == avglist.get(k).getKey()){
                        
                         prodId = differentList.get(i).getKey();
                         agprice = avglist.get(k).getValue();
                         tarprice = targetlist.get(j).getValue();
                         diferen = differentList.get(i).getValue();
                         System.out.println(prodId + "\t\t" + agprice+"\t"+tarprice+"\t\t"+diferen);   
                    }
                    
                } 
            }
            
        }
        */
        
        for (int i = 0; i < avglist.size() && i < targetlist.size(); i++) {
            int productId = differentList.get(i).getKey();
            double avgprice = avglist.get(i).getValue();
            int tprice = targetlist.get(i).getValue();
            double differen = differentList.get(i).getValue();
            
            System.out.println(productId + "\t\t" + avgprice+"\t"+tprice+"\t\t"+differen);
        }
       
        System.out.println("--------------------------------------------------------------------------------");
        
        
        //************************ Displaying the separate Sections for Original Data ***********************
        
        System.out.println("\nProducts with Average Sales Price Lower than the Target Price:\n");
        System.out.println("-----------------------------------------------");
        System.out.println("ProductId"+"\t"+"Average Sales Price");
        System.out.println("-----------------------------------------------");
        
        List< Entry< Integer, Double>> originalLowerList = new ArrayList<>(entrySet7);
        List< Entry< Integer, Double>> originalHigherList = new ArrayList<>(entrySet8);
        
        for (int i = 0; i < originalLowerList.size(); i++) {
            int productId = originalLowerList.get(i).getKey();
            double avgprice = originalLowerList.get(i).getValue();
            
            System.out.println(productId + "\t\t" + avgprice);
        }
        System.out.println("*********************************************************************");
        System.out.println("\nProducts with Average Sales Price Higher than the Target Price:\n");
        System.out.println("-----------------------------------------------");
        System.out.println("ProductId"+"\t"+"Average Sales Price");
        System.out.println("-----------------------------------------------");
        
        for (int i = 0; i < originalHigherList.size()  ; i++) {
            
            int productId = originalHigherList.get(i).getKey();
            double avgprice = originalHigherList.get(i).getValue();
            System.out.println(productId + "\t\t" + avgprice);
        }
        
        System.out.println("*********************************************************************");
        
        
        System.out.println("MODIFIED DATA:\n");
        System.out.println("-------------------------------------------------------------------------------------------------------------------------");
        System.out.println("ProductId"+"\t"+"Average Sales Price"+"\t\t"+"Adjusted Target Price"+"\t\t"+"Difference Price"+"\t"+"Error");
        System.out.println("-------------------------------------------------------------------------------------------------------------------------");
        
        List< Entry< Integer, Double>> adjustTargetlist = new ArrayList<>(entrySet4);
        List< Entry< Integer, Double>> adjustDifflist = new ArrayList<>(entrySet5);
        List< Entry< Integer, Double>> errorList = new ArrayList<>(entrySet6);
        
        for (int i = 0; i < adjustTargetlist.size() && i < adjustDifflist.size() ; i++) {
            int productId = avglist.get(i).getKey();
            double avgprice = avglist.get(i).getValue();
            double adjTprice = adjustTargetlist.get(i).getValue();
            double adjTdifferen = adjustDifflist.get(i).getValue();
            double errorP = errorList.get(i).getValue();
            System.out.println(productId + "\t\t" + avgprice+"\t\t"+adjTprice+"\t\t"+adjTdifferen+"\t"+errorP);
        }
        
        System.out.println("-------------------------------------------------------------------------------------------------------------------------");
        
         //************************ Displaying the Sections for Original Data ***********************
        
        System.out.println("\nProducts with Average Sales Price Lower than the Modified Target Price:\n");
        System.out.println("ProductId"+"\t"+"Average Sales Price");
        System.out.println("-----------------------------------------------");
        
        List< Entry< Integer, Double>> modLowerList = new ArrayList<>(entrySet9);
        List< Entry< Integer, Double>> modHigherList = new ArrayList<>(entrySet10);
        
        for (int i = 0; i < modLowerList.size(); i++) {
            
            int productId = modLowerList.get(i).getKey();
            double avgprice = modLowerList.get(i).getValue();
            System.out.println(productId + "\t\t" + avgprice);
        }
        
        System.out.println("*********************************************************************");
        System.out.println("\nProducts with Average Sales Price Higher than the Modified Target Price:\n");
        System.out.println("ProductId"+"\t"+"Average Sales Price");
        System.out.println("-----------------------------------------------");
        
        for (int i = 0; i < modHigherList.size()  ; i++) {
            
            int productId = modHigherList.get(i).getKey();
            double avgprice = modHigherList.get(i).getValue();
            System.out.println(productId + "\t\t" + avgprice);
        }
        
        System.out.println("*********************************************************************");
       
     }
}
