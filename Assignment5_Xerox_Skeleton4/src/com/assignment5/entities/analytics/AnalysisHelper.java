/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.assignment5.entities.analytics;

import com.assignment5.entities.Order;
import com.assignment5.entities.Product;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

/**
 *
 * @author Team Void
 */
public class AnalysisHelper {

    public void threeBestNegotiatedProducts() {

        Map< Integer, Product> productsList = DataStore.getInstance().getProduct();
        Map< Integer, Order> orderMap = DataStore.getInstance().getOrder();
        Map< Integer, Integer> result = new HashMap<>();

        for (Order order : orderMap.values()) {

            int count = 0;
            int productID = order.getItem().getProductId();
            int target = 0;
            int quant = order.getItem().getQuantity();

            for (Product p : productsList.values()) {
                if (p.getProductId() == productID) {
                    target = p.getTargetPrice();
                }
            }

            if (order.getItem().getSalesPrice() > target) {

                if (result.containsKey(productID)) {

                    count = result.get(productID); //returns the value for given key
                    quant += count;

                }

                result.put(productID, quant);
            }
        }

        Set entrySet = result.entrySet();
        List< Entry< Integer, Integer>> list = new ArrayList<>(entrySet);
        Collections.sort(list, new Comparator< Entry< Integer, Integer>>() {

            public int compare(Entry< Integer, Integer> a, Entry< Integer, Integer> b) {

                return b.getValue() - a.getValue();
            }
        });
        System.out.println("*********************************************************************");
        System.out.println("Top three negotiated products:");

        for (int i = 0; i < list.size() && i < 3; i++) {
            int productId = list.get(i).getKey();
            int quant = list.get(i).getValue();
            System.out.println("ProductID: " + productId + " Quantity: " + quant);
        }

        System.out.println("*********************************************************************");

    }

    public void topThreeCustomers() {

        Map< Integer, Product> productsList = DataStore.getInstance().getProduct();
        Map< Integer, Order> orderMap = DataStore.getInstance().getOrder();
        Map< Integer, Integer> result = new HashMap<>();

        for (Order order : orderMap.values()) {

            int count = 0;
            int productID = order.getItem().getProductId();
            int customerID = order.getCustomerId();
            int sales = order.getItem().getSalesPrice();
            int target = 0;

            for (Product p : productsList.values()) {
                if (p.getProductId() == productID) {
                    target = p.getTargetPrice();
                }
            }
            int diff = Math.abs(sales - target);
            //  int quant = diff * order.getItem().getQuantity();

            //if (order.getItem().getSalesPrice() > target)
            //{
            if (result.containsKey(customerID)) {

                count = result.get(customerID); //returns the value for given key
                diff += count;

            }

            result.put(customerID, diff);
            //}
        }

        Set entrySet = result.entrySet();
        List< Entry< Integer, Integer>> list = new ArrayList<>(entrySet);
        Collections.sort(list, new Comparator< Entry< Integer, Integer>>() {
            public int compare(Entry< Integer, Integer> a, Entry< Integer, Integer> b) {

                return b.getValue() - a.getValue();
            }
        });
        System.out.println("Top three customers:");

        for (int i = 0; i < list.size() && i < 3; i++) {
            int custID = list.get(i).getKey();
            int quant = list.get(i).getValue();
            System.out.println("CustomerID: " + custID + " Profit: " + quant);
        }

        System.out.println("*********************************************************************");
    }
    
    public void topThreeSales() {

        Map< Integer, Product> productsList = DataStore.getInstance().getProduct();
        Map< Integer, Order> orderMap = DataStore.getInstance().getOrder();
        Map< Integer, Integer> result = new HashMap<>();

        for (Order order : orderMap.values()) {

            int count = 0;
            int productID = order.getItem().getProductId();
            int salesID = order.getSupplierId();
            int sales = order.getItem().getSalesPrice();
            int target = 0;

            for (Product p : productsList.values()) {
                if (p.getProductId() == productID) {
                    target = p.getTargetPrice();
                }
            }
            int diff = sales - target;
            int quantsales=0;
            quantsales = diff * order.getItem().getQuantity();
            if(diff>0)
            {
            if (result.containsKey(salesID)) {

                count = result.get(salesID); //returns the value for given key
                //diff += count;
                quantsales +=count;
            }

            result.put(salesID, quantsales);
            }
        }

        Set entrySet = result.entrySet();
        List< Entry< Integer, Integer>> list = new ArrayList<>(entrySet);
        Collections.sort(list, new Comparator< Entry< Integer, Integer>>() {
            @Override
            public int compare(Entry< Integer, Integer> a, Entry< Integer, Integer> b) {

                return b.getValue() - a.getValue();
            }
        });
        System.out.println("Top three salesperson:");

        for (int i = 0; i < list.size() && i < 3; i++) {
            int salesID = list.get(i).getKey();
            int quantsales = list.get(i).getValue();
            System.out.println("Sales ID: " + salesID + " Profit: " + quantsales);
        }

        System.out.println("*********************************************************************");
    }
    
    //4th 
    
    
    public void totalrevenue() {

        Map< Integer, Product> productsList = DataStore.getInstance().getProduct();
        Map< Integer, Order> orderMap = DataStore.getInstance().getOrder();
        Map< Integer, Integer> result = new HashMap<>();

        for (Order order : orderMap.values()) {

            int count = 0;
            int productID = order.getItem().getProductId();
            int salesID = order.getSupplierId();
            int sales = order.getItem().getSalesPrice();
            int target = 0;

            for (Product p : productsList.values()) {
                if (p.getProductId() == productID) {
                    target = p.getTargetPrice();
                }
            }
            int diff = sales - target;
             int quantsales = diff * order.getItem().getQuantity();
            if(diff>0)
            {
            if (result.containsKey(salesID)) {

                count = result.get(salesID); //returns the value for given key
                //diff += count;
                quantsales +=count;
            }

            result.put(salesID, quantsales);
            }
        }

        Set entrySet = result.entrySet();
        List< Entry< Integer, Integer>> list = new ArrayList<>(entrySet);
        Collections.sort(list, new Comparator< Entry< Integer, Integer>>() {
            @Override
            public int compare(Entry< Integer, Integer> a, Entry< Integer, Integer> b) {

                return b.getValue() - a.getValue();
            }
        });
        System.out.println("Total revenue:");
        int quantsalesfinal=0;

        for (int i = 0; i < list.size(); i++) {
          //  int salesID = list.get(i).getKey();
            int quantsales = list.get(i).getValue();
            quantsalesfinal= quantsalesfinal +quantsales;   
        }
         System.out.println("Total revenue is " + quantsalesfinal);
        System.out.println("*********************************************************************");
    }

}
