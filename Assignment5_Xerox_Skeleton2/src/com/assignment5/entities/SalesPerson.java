/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.assignment5.entities;

import java.util.List;

/**
 *
 * @author Team Void
 */
public class SalesPerson {
    
    private int salesId;
 //   private int salesPerProduct;
    private int marketSegment;
    
    private List<Product> productList;
    

    public int getSalesId() {
        return salesId;
    }

    public void setSalesId(int salesId) {
        this.salesId = salesId;
    }

/*    public int getSalesPerProduct() {
        return salesPerProduct;
    }

    public void setSalesPerProduct(int salesPerProduct) {
        this.salesPerProduct = salesPerProduct;
    }*/

    public int getMarketSegment() {
        return marketSegment;
    }

    public void setMarketSegment(int marketSegment) {
        this.marketSegment = marketSegment;
    }

    public List<Product> getProductList() {
        return productList;
    }

    public void setProductList(List<Product> productList) {
        this.productList = productList;
    }
    
    
    
    
    
}
