/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.assignment5.entities.analytics;

import com.assignment5.entities.Customer;
import com.assignment5.entities.Order;
import com.assignment5.entities.Product;
import com.assignment5.entities.SalesPerson;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

/**
 *
 * @author Team Void
 */
public class AnalysisHelper {
    public void checkOrder()
    {
     Map< Integer, Order> orderMap = DataStore.getInstance().getOrder();
    for (Order order : orderMap.values()) {

            System.out.println("Order ID "+order.getOrderId() + "Product ID "+ order.getItem().getProductId()+ " Quantity " + order.getItem().getQuantity()+ " Custimer id "+ order.getCustomerId()+" Sales Price "+ order.getItem().getSalesPrice());
    }
    }

    public void threeBestNegotiatedProducts() {

        Map< Integer, Product> productsList = DataStore.getInstance().getProduct();
        Map< Integer, Order> orderMap = DataStore.getInstance().getOrder();
        Map< Integer, Integer> result = new HashMap<>();

        for (Order order : orderMap.values()) {

            int count = 0;
            int productID = order.getItem().getProductId();
            int target = 0;
            int quant = order.getItem().getQuantity();

            for (Product p : productsList.values()) {
                if (p.getProductId() == productID) {
                    target = p.getTargetPrice();
                }
            }

            if (order.getItem().getSalesPrice() > target) {

                if (result.containsKey(productID)) {

                    count = result.get(productID); //returns the value for given key
                    quant += count;

                }

                result.put(productID, quant);
            }
        }

        Set entrySet = result.entrySet();
        List< Entry< Integer, Integer>> list = new ArrayList<>(entrySet);
        Collections.sort(list, new Comparator< Entry< Integer, Integer>>() {

            public int compare(Entry< Integer, Integer> a, Entry< Integer, Integer> b) {

                return b.getValue() - a.getValue();
            }
        });
        System.out.println("*********************************************************************");
        System.out.println("Top three negotiated products:");

        for (int i = 0; i < list.size() && i < 3; i++) {
            int productId = list.get(i).getKey();
            int quant = list.get(i).getValue();
            System.out.println("ProductID: " + productId + " Quantity: " + quant);
        }

        System.out.println("*********************************************************************");

    }

    public void topThreeCustomers() {

        Map< Integer, Product> productsList = DataStore.getInstance().getProduct();
        Map< Integer, Integer> result = new HashMap<>();
        Map< Integer, Order> orderMap = DataStore.getInstance().getOrder();
       
       
        
        for (Order order : orderMap.values()) {

            int count = 0;
            int productID = order.getItem().getProductId();
            int customerID = order.getCustomerId();
            int sales = order.getItem().getSalesPrice();
            int target = 0;

            for (Product p : productsList.values()) {
                if (p.getProductId() == productID) {
                    target = p.getTargetPrice();
                }
            }
            int diff = Math.abs(sales - target);
            
            if (result.containsKey(customerID)) {

                count = result.get(customerID); //returns the value for given key
                diff += count;

            }
                result.put(customerID, diff);
               // System.out.println("Display"+result);
            
        }

        Set entrySet = result.entrySet();
        List< Entry< Integer, Integer>> list = new ArrayList<>(entrySet);
        Collections.sort(list, new Comparator< Entry< Integer, Integer>>() {
            public int compare(Entry< Integer, Integer> a, Entry< Integer, Integer> b) {

                return a.getValue() - b.getValue();
            }
        });
        System.out.println("Top three customers:");

        for (int i = 0; i < list.size() && i < 3; i++) {
            int custID = list.get(i).getKey();
            int quant = list.get(i).getValue();
            System.out.println("CustomerID: " + custID + " Profit: " + quant);
        }

        System.out.println("*********************************************************************");
    }
    
    public void topThreeSales() {

        Map< Integer, Product> productsList = DataStore.getInstance().getProduct();
        Map< Integer, Order> orderMap = DataStore.getInstance().getOrder();
       
        Map< Integer, Integer> result = new HashMap<>();

        
        
        for (Order order : orderMap.values()) {

            int count = 0;
            int productID = order.getItem().getProductId();
            int salesID = order.getSupplierId();
            int sales = order.getItem().getSalesPrice();
            int target = 0;

            for (Product p : productsList.values()) {
                if (p.getProductId() == productID) {
                    target = p.getTargetPrice();
                }
            }
            int diff = sales - target;
            int quantsales=0;
            quantsales = diff * order.getItem().getQuantity();
            
            
            if(diff>0)
            {
            if (result.containsKey(salesID)) {

                count = result.get(salesID); //returns the value for given key
                quantsales +=count;
            }

            result.put(salesID, quantsales);
            }
        }
        

        Set entrySet = result.entrySet();
        List< Entry< Integer, Integer>> list = new ArrayList<>(entrySet);
        Collections.sort(list, new Comparator< Entry< Integer, Integer>>() {
            @Override
            public int compare(Entry< Integer, Integer> a, Entry< Integer, Integer> b) {

                return b.getValue() - a.getValue();
            }
        });
        System.out.println("Top three salesperson:");

        for (int i = 0; i < list.size() && i < 3; i++) {
            int salesID = list.get(i).getKey();
            int quantsales = list.get(i).getValue();
            System.out.println("Sales ID: " + salesID + " Profit: " + quantsales);
        }

        System.out.println("*********************************************************************");
    }
     
    
    
    public void totalrevenue() {

        Map< Integer, Product> productsList = DataStore.getInstance().getProduct();
        Map< Integer, Order> orderMap = DataStore.getInstance().getOrder();
       // Map< Integer, Integer> targetPrice = new HashMap<>();
        //Map< Integer, SalesPerson> salesMap = DataStore.getInstance().getSalesPerson();
        Map< Integer, Integer> result = new HashMap<>();
         
         
         
         for (Order order : orderMap.values()) {

            int count = 0;
            int productID = order.getItem().getProductId();
            int salesID = order.getSupplierId();
            int sales = order.getItem().getSalesPrice();
            int target = 0;

            for (Product p : productsList.values()) {
                if (p.getProductId() == productID) {
                    target = p.getTargetPrice();
                }
            }
            int diff = sales - target;
            int quantsales = diff * order.getItem().getQuantity();
            
            if(diff>0)
            {
            if (result.containsKey(salesID)) {

                count = result.get(salesID); //returns the value for given key
                quantsales +=count;
            }

            result.put(salesID, quantsales);
            }
         }
        

        Set entrySet = result.entrySet();
        List< Entry< Integer, Integer>> list = new ArrayList<>(entrySet);
        Collections.sort(list, new Comparator< Entry< Integer, Integer>>() {
            @Override
            public int compare(Entry< Integer, Integer> a, Entry< Integer, Integer> b) {

                return b.getValue() - a.getValue();
            }
        });
        System.out.println("Total revenue:");
        int quantsalesfinal=0;

        for (int i = 0; i < list.size(); i++) {
          //  int salesID = list.get(i).getKey();
            int quantsales = list.get(i).getValue();
            quantsalesfinal= quantsalesfinal + quantsales;   
        }
         System.out.println("Total revenue is " + quantsalesfinal);
        System.out.println("*********************************************************************");
    }

     public void finalMethod(){
         
         Map< Integer, Product> productsMap = DataStore.getInstance().getProduct();
         Map< Integer, Order> orderMap = DataStore.getInstance().getOrder();
         Map< Integer, Float> avgMap = new HashMap<>();
         Map< Integer, Integer> targetMap = new HashMap<>();
         Map< Integer, Float> differenceMap = new HashMap<>();
         Map< Integer, Double> adjustedTargetMap = new HashMap<>();
         Map< Integer, Double> adjustedDifferenceMap = new HashMap<>();
         Map< Integer, Double> errorMap = new HashMap<>();

             for (Product p : productsMap.values()) {
                 
             float sumSales = 0;
             float sumQuantity = 0;
             
             for(Order o: orderMap.values()){
                   
                if (p.getProductId() == o.getItem().getProductId()) {
                
                    sumSales += o.getItem().getSalesPrice() * o.getItem().getQuantity();
                    sumQuantity += o.getItem().getQuantity();
                    
                }   
               }
             float difference = 0;
             float averagePrice = 0;
             averagePrice = sumSales/sumQuantity;
             difference = averagePrice - p.getTargetPrice();
             avgMap.put(p.getProductId(), averagePrice);
             targetMap.put(p.getProductId(), p.getTargetPrice()); 
             differenceMap.put(p.getProductId(), difference);
             
             //*********************** Modified Data ***********************
        
            double adjustedTarget = 0;
            
            if(p.getTargetPrice() < (averagePrice - (averagePrice * 0.05))){
                
                adjustedTarget = averagePrice - (averagePrice * 0.05);
                adjustedTargetMap.put(p.getProductId(), adjustedTarget);
            }
            
            else{
                
                adjustedTarget = averagePrice + (averagePrice * 0.05);
                adjustedTargetMap.put(p.getProductId(), adjustedTarget);
            }
            
            double adjustedDiff =0;
            adjustedDiff = averagePrice - adjustedTarget;
            double errorValue = 0;
            
            errorValue = (adjustedTarget - averagePrice)/averagePrice;
            adjustedDifferenceMap.put(p.getProductId(), adjustedDiff);
            errorMap.put(p.getProductId(), errorValue);
            }  
             
        //************************Display Table Logic**********************
        
        System.out.println("ORIGINAL DATA:\n");
        System.out.println("-------------------------------------------------------------------------------");
        System.out.println("ProductId"+"\t"+"Average Sales Price"+"\t"+"Target Price"+"\t"+"Difference Price");
        System.out.println("-------------------------------------------------------------------------------");
        
        Set entrySet1 = avgMap.entrySet();
        Set entrySet2 = targetMap.entrySet();
        Set entrySet3 = differenceMap.entrySet();
        
        //*************** Modified Table ****************
        Set entrySet4 = adjustedTargetMap.entrySet();
        Set entrySet5 = adjustedDifferenceMap.entrySet();
        Set entrySet6 = adjustedDifferenceMap.entrySet();
        
        
        List< Entry< Integer, Float>> avglist = new ArrayList<>(entrySet1);
        List< Entry< Integer, Integer>> targetlist = new ArrayList<>(entrySet2);
        List< Entry< Integer, Float>> differentList = new ArrayList<>(entrySet3);
        
        for (int i = 0; i < avglist.size() && i < targetlist.size() ; i++) {
            int productId = avglist.get(i).getKey();
            float avgprice = avglist.get(i).getValue();
            int tprice = targetlist.get(i).getValue();
            float differen = differentList.get(i).getValue();
            System.out.println(productId + "\t\t" + avgprice+"\t\t"+tprice+"\t\t"+differen);
        }
       
        System.out.println("*********************************************************************");

        System.out.println("MODIFIED DATA:\n");
        System.out.println("-------------------------------------------------------------------------------------------------------------");
        System.out.println("ProductId"+"\t"+"Average Sales Price"+"\t"+"Adjusted Target Price"+"\t"+"Difference Price"+"\t"+"Error");
        System.out.println("-------------------------------------------------------------------------------------------------------------");
        
        List< Entry< Integer, Double>> adjustTargetlist = new ArrayList<>(entrySet4);
        List< Entry< Integer, Double>> adjustDifflist = new ArrayList<>(entrySet5);
        List< Entry< Integer, Double>> errorList = new ArrayList<>(entrySet6);
        
        for (int i = 0; i < adjustTargetlist.size() && i < adjustDifflist.size() ; i++) {
            int productId = avglist.get(i).getKey();
            float avgprice = avglist.get(i).getValue();
            double adjTprice = adjustTargetlist.get(i).getValue();
            double adjTdifferen = adjustDifflist.get(i).getValue();
            double errorP = errorList.get(i).getValue();
            System.out.println(productId + "\t\t" + avgprice+"\t\t"+adjTprice+"\t"+adjTdifferen+"\t"+errorP);
        }
     }
}
